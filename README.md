# Image Enhancement and Filtering

## Image Processing course assignment 2.

Authors:
  - João Pedro Ramos Belmiro

Folders and files:
  - Ex2 code contains the Python code used for run.codes submission
  - inputs contains images used in the demos
  - Assignment2.ipynb is a notebook exemplifying functions developed and submitted
