"""
João Pedro Ramos Belmiro
9791198
BCC
2020.1

Assignment 2: Image Enhancement and Filtering
git: https://gitlab.com/jprbelmiro/image-enhancement-and-filtering.git
"""

import math
import numpy as np
import imageio


#Padding Logic
def PadImage(img, n):
    n_half = math.floor(n/2)
    paddedImg = np.pad(img, ((n_half,n_half),(n_half,n_half)), mode = "constant")
    return paddedImg

def CoordinateInPaddedImg(x, y, n):
    n_half = math.floor(n/2)
    xPad = x+n_half
    yPad = y+n_half
    
    return xPad, yPad

##Auxiliar matematical funcitions
def Euclidian(x, y):
    return np.sqrt(pow(x, 2) + pow(y, 2))

def Gaussian(x, sigma):
    gaussian = (1/(2*np.pi*(sigma**2)))*np.exp(-x**2/(2 * (sigma**2)))
    return gaussian

def Scale(img):
    min = np.min(img)
    max = np.max(img)
    scaledImg = ((img - min) * 255)/(max-min)
    return scaledImg

def RSE(m, r):
    r = np.float64(r)
    error_matrix = np.power(m - r, 2)
    return np.sqrt(np.sum(error_matrix))

#==========================FUNCION 1==========================
##Calculate kernel for spatial Gaussian
def SpatialGaussian(sigma, n):
    n_half = math.floor(n/2)
    gaussianKernel = np.zeros(shape=(n,n), dtype=np.float64)
    
    for i in range(-n_half, n_half + 1):
        for j in range(-n_half, n_half + 1):
            gaussianKernel[i+n_half][j+n_half] = Gaussian(Euclidian(i, j), sigma)

    return gaussianKernel
    
##Calculate kernel for range Gaussian
def RangeGaussian(img, x , y, sigma, n):
    n_half = math.floor(n/2)
    gaussianKernel = np.zeros(shape=(n,n), dtype=np.float64)
    xPad, yPad = CoordinateInPaddedImg(x, y, n)
    
    for i in range(-n_half, n_half + 1):
        for j in range(-n_half, n_half + 1):
            gaussianKernel[i+n_half][j+n_half] = Gaussian(img[xPad+i][yPad+j]-img[xPad][yPad], sigma)
    return gaussianKernel

def UpdateFilterImage(fImg, img, wi, x, y, n):
    n_half = math.floor(n/2)
    xPad, yPad = CoordinateInPaddedImg(x, y, n)
    
    for i in range(-n_half, n_half + 1):
        for j in range(-n_half, n_half + 1):
            fImg[x][y] += wi[i+n_half][j+n_half]*img[xPad+i][yPad+j]
                

def BilateralFilter(img, sigmaS, sigmaR, n):
    img = np.float64(img)
    paddedImg = PadImage(img, n)
    fImg = np.zeros(shape = img.shape, dtype=np.float64)
    sG = SpatialGaussian(sigmaS, n)
    for x in range(0, img.shape[0]):
        for y in range(0, img.shape[1]):
            Wp = 0.0
            rG = RangeGaussian(paddedImg, x, y, sigmaR, n)
            wi = sG*rG
            Wp = np.sum(wi, dtype=np.float64)
            UpdateFilterImage(fImg, paddedImg, wi, x, y, n)
            fImg[x][y] = fImg[x][y]/Wp
    return fImg
    
#==========================FUNCION 2==========================
kernel1 = np.array([[0, -1, 0],
           [-1, 4, -1],
           [0, -1, 0]], dtype=np.float64)
           
kernel2 = np.array([[-1, -1, -1],
           [-1, 8, -1],
           [-1, -1, -1]], dtype=np.float64)


def convolve(x, y, If, img, mask):
    n_half = math.floor((mask.shape[0])/2)
    xPad, yPad = CoordinateInPaddedImg(x, y, mask.shape[0])
    
    for i in range(0, mask.shape[0]):
        for j in range(0, mask.shape[1]):
            k = xPad + i - n_half
            l = yPad + j - n_half
            If[x][y] += img[k][l]*mask[i][j]
                
def convolution(If, img, mask):
    for i in range(0, If.shape[0]):
        for j in range(0, If.shape[1]):
            convolve(i, j, If, img, mask)

def LaplacianConvolve(img, c, k):
    if k == 1:
        K = kernel1
    elif k == 2:
        K = kernel2
      
    img = np.float64(img)
    paddedImg = PadImage(img, K.shape[0])
    If = np.zeros(shape=img.shape, dtype=np.float64)
    
    convolution(If, paddedImg, K)
    scaledIf = Scale(If)
    f = (c * scaledIf)
    f = f + img
    scaledF = Scale(f)
    
    return scaledF

#==========================FUNCION 3==========================
def VignetteGaussianKernel(sigma, n):
    n_half = n/2 - 1 if n % 2 == 0 else math.floor(n/2)
    Kernel = np.zeros(shape = n, dtype=np.float64)
    
    for i in range(0, n):
        Kernel[i] = Gaussian(i - n_half, sigma)
    
    return Kernel

def MultiplyVignetteKernels(rowKernel, colKernel):
    rowKernel = rowKernel.reshape((-1, 1))
    colKernel = colKernel.reshape((1, -1))
    return np.matmul(rowKernel,colKernel)
    
def VignetteFilter(img, sigmaR, sigmaC):
    img = np.float64(img)
    
    Wr = VignetteGaussianKernel(sigmaR, img.shape[0])
    Wc = VignetteGaussianKernel(sigmaC, img.shape[1])
    If = MultiplyVignetteKernels(Wr, Wc) * img
    If = Scale(If)
    
    return If
    
#============================MAIN=============================
if __name__ == '__main__':
    #Reading input patterns for exercice
    filename = str(input()).rstrip()
    inputImg = imageio.imread(filename)
    method = int(input())
    save = int(input())
    #switch for diferent methods
    if method == 1:
        filterSize = int(input())
        sigmaS = float(input())
        sigmaR = float(input())
        outputImg = BilateralFilter(inputImg, sigmaS, sigmaR, filterSize)

    if method == 2:
        c = float(input())
        k = int(input())
        outputImg = LaplacianConvolve(inputImg, c, k)
        
    if method == 3:
        sigmaR = float(input())
        sigmaC = float(input())
        outputImg = VignetteFilter(inputImg, sigmaR, sigmaC)
        
    if save == 1:
        imageio.imwrite('outputs/output_img.png', np.uint8(outputImg))

    #printing resulting error
    print("%.4f" % RSE(outputImg, inputImg))
